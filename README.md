# PyTunes Music Player
<p align="center">
  <img src="logo.png" alt="PyTune Logo" width="200"/>
</p>

PyTunes is a lightweight, simple music player with an oldschool look, developed in Python using `tkinter` and `pygame`. It supports **.mp3, .wav** and **.ogg** file formats.


## Look & Features

<p align="center">
  <img src="pytunes_ver2.png" alt="PyTunes" width="400"/>
</p>

Note: The GUI is still an early WIP.


## Installation

To ensure that anyone can set up and run PyTunes, regardless of their initial setup, the following instructions will guide users through installing `Python`, `pip`, `tkinter`, and `pygame`, along with setting up a **virtual environment**. This guide is tailored for _Unix_ and _GNU/Linux_ users.

### Prerequisites

Before installing **PyTunes**, you need to have `Python` and `pip` installed on your system. **PyTunes** has been developed and tested with `Python 3`. Here's how to get everything set up:

1. **Installing Python**

   If Python is not already installed on your system, you can download it from the official Python website or install it using your distribution's package manager. **MacOS** users may need to use `Homebrew` for some of these steps.

   For **Ubuntu** and **Debian**-based systems:
   ```bash
   sudo apt update
   sudo apt install python3 python3-pip python3-dev
   ```
 For **Fedora** and other RPM-based systems:

 ```bash
sudo dnf install python3 python3-pip
 ```

 ### Installing Tkinter

Tkinter is Python's standard GUI (Graphical User Interface) package. It is typically included with Python, but if it's not, you can install it as follows:

- For **Ubuntu** and **Debian**-based systems:

  ```bash
  sudo apt install python3-tk 
  ```

For **Fedora** and other RPM-based systems:
 ```bash
 sudo dnf install python3-tkinter  
```
## Setting Up a Virtual Environment

It's recommended to use a virtual environment to manage the dependencies for this program. This keeps dependencies required by different projects separate by creating isolated environments for them.

```bash
python3 -m venv pytunes-env
source pytunes-env/bin/activate
```
## Installing PyTunes Dependencies

After setting up and activating the virtual environment, you'll need to install PyTunes's dependencies. This includes **pygame**, which is used for music playback, and any other packages listed in requirements.txt.
```bash
sudo apt install python3 python3-pip
pip install pygame
pip install Pillow
pip install mutagen
```

## Install Other Dependencies

Ensure you are in the project directory where requirements.txt is located, then run:

    pip install -r requirements.txt

## Running PyTunes

With Python, pip, Tkinter, and Pygame installed, and after setting up your virtual environment and installing the dependencies, you're ready to run PyTunes.

Navigate to the directory containing PyTunes.py and execute:

```bash
python PyTunes.py
```
for some users, they may instead need to type:
```bash
python3 PyTunes.py
```
~~Remember to activate your virtual environment (source pytune-env/bin/activate) whenever you work on the project.~~

## Roadmap & Status of project
On hiatus, but will continue project again, soon. Upcoming features include but are not limited to:

#### Prioirty
- [x] Better skin 
- [x] Song Metadata Display
- [x] Album art
- [ ] Play count for each song
- [x] Visualizer
- [ ] Que songs
- [x] Load user's entire music collection into PyTunes, using their Music directory 
- [x] Open by default in Music directory
- [x] Random shuffle
- [x] Repeat one (bonus: repeat 3x, 10x times exist as options too now)
- [ ] Favorites
- [ ] Star ratings
- [x] Continue playing to next song
- [x] Clear/remove song(s)
- [ ] Highlight buttons when hovered over/clicked.
- [ ] Double clicking selected song makes it play
- [ ] Add detailed error logger
- [ ] Add clickable mascot 
- [ ] Make song titles show up in the listbox, correctly
- [ ] Theme switcher
- [ ] Simple equalizer
- [ ] Audio effects

#### Might implement eventually
**Anything crossed out is very not likely to ever be implemented

- [ ] Mute button
- [ ] Change playback speed
- [ ] Add midi support
- [ ] Add flac support
- [ ] Open recent
- [ ] Sleep timer that self stops music after a certain time
- [ ] Drag and drop audio files
- [ ] Playlists
- [ ] Let user select their default directory on their own for PyTunes to remember
- [ ] Reverse songs
- [ ] Sort by...
- [ ] Fix how song names appear in the music list box
- [ ] Add timestamps to show how long each song is
- [ ] ~~Show Artists tab~~
- [ ] ~~Show Album tab~~
- [ ] ~~Scroll thru album art~~
- [ ] ~~Top played songs section~~
- [ ] ~~Add record option~~
- [ ] ~~Add more audio formats~~
- [ ] ~~Fade out smooth transition~~
- [ ] ~~Pop ups~~
- [ ] ~~Song duration display~~
- [ ] ~~Tiling image background~~
- [ ] ~~More visually appealing audio meter~~
- [ ] ~~Song progress~~
- [ ] ~~Remembering Playback Position between sessions~~

## Thanks

Special thanks to all my friends who tested and reported to me any issues they encountered. Big kudos to **ChatGPT** for helping correct a _plethora_ of my coding mistakes.

## Extra comments & trivia from Author
This is to become my lenghtiest project and remain the longest for a long while. I will stick to significantly shorter projects when I am not working on PyTunes. (02-03-2024)


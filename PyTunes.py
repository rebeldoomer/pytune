import pygame
from pygame import mixer, USEREVENT
import tkinter as tk
from tkinter import filedialog
from PIL import Image, ImageTk
import mutagen
from mutagen.mp3 import MP3
from mutagen.id3 import ID3, APIC, error
from io import BytesIO
import json
import os
import random
import logging
# from datetime import datetime

# Configure logging
logging.basicConfig(filename='pytunes_errors.log', level=logging.ERROR,
                    format='%(asctime)s %(levelname)s:%(message)s')

# Example usage
try:
    # Potentially problematic code here
    # For example, open a file that doesn't exist
    with open('non_existent_file.txt', 'r') as file:
        pass
except Exception as e:
    logging.error("Failed to open file: %s", e)

try:
    logo_image = Image.open('logo.png')
    # Further processing...
except Exception as e:
    print(f"Error loading logo image: {e}")
    # Handle the error (e.g., use a default image or skip setting the image)

class MusicPlayer:
    def create_visualizer(self):
        self.visualizer_frame = tk.Frame(self.root, bg='black')
        self.visualizer_frame.pack(padx=15, pady=15, fill=tk.X)

        # Reduce the height of the canvas for the visualizer here
        self.canvas_height = 100  # Height of the canvas
        self.canvas_width = 300   # Width of the canvas, set to the desired value
        self.canvas = tk.Canvas(self.visualizer_frame, bg='#353a4c', width=self.canvas_width,
                        height=self.canvas_height)
        self.canvas.pack()

        self.bars = []
        self.max_bar_height = self.canvas_height - 20  # Ensure the max height of bars is less than the canvas height
        self.num_bars = 50  # The number of bars can be adjusted for desired visual effect

        # Calculate total width for bars including the gaps
        total_gap_space = (self.num_bars - 1)  # One pixel gap for each bar except the last one
        total_bar_width = self.playlist_box.winfo_reqwidth() - total_gap_space
        self.bar_width = total_bar_width / self.num_bars

        self.bar_color = '#00b386'  # Color of the bars, same as button background
        self.trail_color = '#353a4c'  # Color of the trail 'boxes'

        # Create the bars
        for i in range(self.num_bars):
            bar_height = random.randint(20, self.max_bar_height)
            bar_x0 = i * (self.bar_width + 1)  # Start point for each bar including one pixel gap
            bar_x1 = bar_x0 + self.bar_width  # End point for each bar
            bar_y0 = self.canvas_height - bar_height  # Start point for bar height
            bar_y1 = self.canvas_height  # End point at bottom of the canvas
            bar = self.canvas.create_rectangle(bar_x0, bar_y0, bar_x1, bar_y1, fill=self.bar_color, outline='')
            self.bars.append(bar)

        self.is_visualizer_running = False




    def start_visualizer(self):
        if self.visualizer_after_id:  # Cancel the existing scheduled update if there is one
            self.root.after_cancel(self.visualizer_after_id)
        self.is_visualizer_running = True
        self.update_visualizer()

    def stop_visualizer(self):
        if self.visualizer_after_id:  # Cancel the existing scheduled update if there is one
            self.root.after_cancel(self.visualizer_after_id)
        self.is_visualizer_running = False

    def update_visualizer(self):
        if not self.is_visualizer_running:
            # If the visualizer is not supposed to run, we exit the function.
            return

        trail_length = 5  # Determines how many 'trail boxes' are visible
        bar_gap = 2  # The gap between bars

        for i, bar in enumerate(self.bars):
            current_top, current_bottom = self.canvas.coords(bar)[1], self.canvas.coords(bar)[3]
            new_height = random.randint(20, self.max_bar_height)
            new_top = self.canvas.winfo_height() - new_height

            # Adjust the bar height
            self.canvas.coords(bar, i * self.bar_width, new_top,
                               (i + 1) * self.bar_width - bar_gap, current_bottom)

            # Add the trail rectangles if the bar is shrinking
            if new_top > current_top:
                # Calculate the trail box heights and place them
                for j in range(1, trail_length + 1):
                    trail_top = current_top + (self.max_bar_height / trail_length) * j
                    if trail_top < new_top:
                        self.canvas.create_rectangle(
                            i * self.bar_width, trail_top - bar_gap,
                            (i + 1) * self.bar_width - bar_gap, trail_top,
                            fill=self.trail_color, outline=''
                        )

        # Remove the trail boxes that are beyond the current top of each bar
        self.clean_trails()

        # Stop any existing animation, and start a new one.
        if self.visualizer_after_id:
            self.root.after_cancel(self.visualizer_after_id)
        self.visualizer_after_id = self.root.after(200, self.update_visualizer)  # Reschedule the update.

    def clean_trails(self):
        # This function cleans up trail 'boxes' that are no longer needed
        for bar in self.bars:
            bar_top = self.canvas.coords(bar)[1]
            items = self.canvas.find_overlapping(
                self.canvas.coords(bar)[0], 0,
                self.canvas.coords(bar)[2], bar_top
            )
            for item in items:
                if item not in self.bars:  # Ensure we don't delete the bars themselves
                    self.canvas.delete(item)

    def play_next_song(self):
        if not self.playlist:
            print("The playlist is empty. Please load songs first.")
            return

        if self.repeat_count > 0:
            self.repeat_count -= 1
        else:
            if self.shuffle:
                next_song_index = random.choice([i for i in range(len(self.playlist)) if i != self.current_song_index])
            else:
                next_song_index = (self.current_song_index + 1) % len(self.playlist)

            self.current_song_index = next_song_index  # Update to the next song index

        self.play_song(self.playlist[self.current_song_index])

        def toggle_repeat(self):
            repeat_modes = [0, 1, 3, 10]  # No repeat, once, thrice, ten times
            self.repeat_state = (self.repeat_state + 1) % len(repeat_modes)
            self.repeat_count = repeat_modes[self.repeat_state]
            # Update the UI to reflect the new repeat state if necessary

    def remove_selected_songs(self):
        # Get the selected indices in reverse order
        selected_indices = self.playlist_box.curselection()[::-1]
        if not selected_indices:
            print("Please select a song to remove.")
            return

        for index in selected_indices:
            # Remove from the listbox and the playlist
            self.playlist_box.delete(index)
            del self.playlist[index]

        # Reset the current song index if necessary
        if self.current_song_index in selected_indices:
            self.current_song_index = None
            mixer.music.stop()  # Stop the music if the current song is deleted

    def check_events(self):
        for event in pygame.event.get():
            if event.type == self.SONG_END:
                self.play_next_song()
        # Schedule the check_events method to be called again
        self.root.after(100, self.check_events)

    def __init__(self, root):
        self.root = root
        self.root.title('PyTunes')
        self.root.configure(bg='black')

        # Initialize pygame and mixer
        pygame.init()
        pygame.mixer.init()
        pygame.display.set_mode((1, 1))

        self.play_counts_file = 'play_counts.json'
        self.play_counts = self.load_play_counts()
        self.repeat_state = 0
        self.repeat_count = 0  # repeat songs

        # Load the logo image
        logo_image = Image.open('logo.png').resize((256, 256), Image.LANCZOS)
        self.logo = ImageTk.PhotoImage(logo_image)
        self.root.iconphoto(False, self.logo)

        self.supported_formats = ('.mp3', '.wav', '.ogg')

        mixer.init()
        self.SONG_END = USEREVENT + 1
        mixer.music.set_endevent(self.SONG_END)

        self.visualizer_after_id = None  # Add this line to initialize the ID for after_cancel

        # Initialize a visual playlist
        self.visual_playlist = []

        self.playlist = []
        self.paused = False
        self.current_song_index = None
        self.repeat_one = False
        self.shuffle = False
        self.play_counts = {}

        self.album_art_label = tk.Label(root, bg='black')
        self.album_art_label.pack(padx=10, pady=10)

        self.now_playing_label = tk.Label(root, text="Not playing", bg='black', fg='white')
        self.now_playing_label.pack(pady=10)

        self.playlist_box = tk.Listbox(root, bg="#353a4c", fg="white", selectbackground="#00b386",
                                       selectmode=tk.EXTENDED, height=15, width=50)
        self.playlist_box.pack(padx=15, pady=15)

        # Automatically load music from the /Music directory
        music_directory = os.path.expanduser('~/Music')  # Adjust this path as necessary
        self.load_music_directory(music_directory)

        # Control frame for buttons
        control_frame = tk.Frame(root, bg='black')
        control_frame.pack(pady=10)

        # Buttons will have the same color as the listbox highlight
        button_bg_color = "#00b386"

        self.play_button = tk.Button(control_frame, text="▶", command=self.play_song, bg=button_bg_color, fg="white")
        self.play_button.grid(row=0, column=1, padx=10)

        self.pause_button = tk.Button(control_frame, text="⏸", command=self.toggle_pause, bg=button_bg_color,
                                      fg="white")
        self.pause_button.grid(row=0, column=2, padx=10)

        self.stop_button = tk.Button(control_frame, text="⏹", command=self.stop_song, bg=button_bg_color, fg="white")
        self.stop_button.grid(row=0, column=3, padx=10)

        self.prev_button = tk.Button(control_frame, text="⏮", command=self.prev_song, bg=button_bg_color, fg="white")
        self.prev_button.grid(row=0, column=0, padx=10)

        self.next_button = tk.Button(control_frame, text="⏭", command=self.next_song, bg=button_bg_color, fg="white")
        self.next_button.grid(row=0, column=4, padx=10)

        self.shuffle_button = tk.Button(control_frame, text="Shuffle", command=self.toggle_shuffle, bg=button_bg_color,
                                        fg="white")
        self.shuffle_button.grid(row=0, column=6, padx=10)

        self.repeat_button = tk.Button(control_frame, text="Repeat", command=self.toggle_repeat, bg=button_bg_color,
                                       fg="white")
        self.repeat_button.grid(row=0, column=5, padx=10)

        control_frame.pack(pady=10)

        self.load_songs_button = tk.Button(root, text="Load Tunes", command=self.load_songs, bg="#00b386", fg="white")
        self.load_songs_button.pack(side=tk.TOP, pady=10)

        self.volume_scale = tk.Scale(root, from_=0, to=100, resolution=1, orient=tk.HORIZONTAL, command=self.set_volume,
                                     bg=button_bg_color, fg="white", troughcolor="#003326")
        self.volume_scale.set(mixer.music.get_volume() * 100)
        self.volume_scale.pack()

        self.remove_songs_button = tk.Button(root, text="Remove Selected Songs", command=self.remove_selected_songs,
                                             bg="#d9534f", fg="white")
        self.remove_songs_button.pack(side=tk.TOP, pady=10)

        self.check_events()  # Start the event loop to check for SONG_END events

    def load_music_directory(self, path):
        for root, dirs, files in os.walk(path):
            for file in files:
                if file.endswith(('.mp3', '.ogg', '.wav')):  # Check for multiple file extensions
                    full_path = os.path.join(root, file)
                    self.playlist.append(full_path)
                    self.playlist_box.insert(tk.END, os.path.basename(file))

    def load_play_counts(self):
        try:
            with open(self.play_counts_file, 'r') as f:
                return json.load(f)
        except FileNotFoundError:
            return {}
        except json.JSONDecodeError:
            print("Error: play_counts.json is not valid JSON.")
            return {}

    def get_album_art(self, mp3_file_path):
        try:
            audio = MP3(mp3_file_path, ID3=ID3)
            if audio.tags is None:
                print(f"No tags found in {mp3_file_path}")
                return None
            for tag in audio.tags.values():
                if isinstance(tag, APIC):
                    album_art = Image.open(BytesIO(tag.data))
                    return album_art
        except error as e:
            print(f"Error extracting album art: {e}")
        except mutagen.mp3.HeaderNotFoundError as e:
            print(f"Header not found in MP3 file: {mp3_file_path}")
        return None

    def update_album_art(self, mp3_file_path):
        try:
            album_art = self.get_album_art(mp3_file_path)
            if album_art:
                album_art_resized = album_art.resize((250, 250), Image.LANCZOS)
                self.album_art_image = ImageTk.PhotoImage(album_art_resized)
            else:
                raise FileNotFoundError("Album art not found, using default image.")
        except Exception as e:
            print(f"Error loading album art image: {e}")
            default_art = Image.open('missing.png')  # Make sure to use the correct path to your default image
            default_art_resized = default_art.resize((250, 250), Image.LANCZOS)
            self.album_art_image = ImageTk.PhotoImage(default_art_resized)
        finally:
            # Always update the label to ensure it reflects the current state
            self.album_art_label.config(image=self.album_art_image)
            self.album_art_label.image = self.album_art_image

    def load_songs(self):
        music_directory = os.path.expanduser('~/Music')
        files = filedialog.askopenfilenames(
            title="Select Tunes",
            initialdir=music_directory,
            filetypes=(("Audio Files", "*.mp3 *.wav *.ogg"), ("All Files", "*.*"))
        )
        for song in files:
            self.playlist.append(song)
            self.visual_playlist.append(song)
            self.playlist_box.insert(tk.END, os.path.basename(song))

    def play_song(self, song_path=None):
        if song_path is None:
            selected_song_index = self.playlist_box.curselection()
            if not selected_song_index:
                print("Please select a song to play.")
                return
            self.current_song_index = selected_song_index[0]
            song_path = self.playlist[self.current_song_index]
        else:
            self.current_song_index = self.playlist.index(song_path)

        try:
            mixer.music.load(song_path)
            mixer.music.play()
            self.paused = False
            self.update_song_metadata(song_path)  # Call to update metadata
            self.update_album_art(song_path)  # Call to update album art
        except Exception as e:
            print(f"Error playing {song_path}: {e}")
        # Update the playlist selection
        self.playlist_box.selection_clear(0, tk.END)
        self.playlist_box.selection_set(self.current_song_index)
        self.playlist_box.activate(self.current_song_index)

        self.start_visualizer()
        # Update the playlist selection ...

    def update_song_metadata(self, song_path):
        # Initialize default values
        artist = "Unknown Artist"
        title = "Unknown Title"
        try:
            audio = MP3(song_path, ID3=ID3)
            if audio.tags is not None:
                if "TPE1" in audio.tags:
                    artist = audio.tags["TPE1"].text[0]
                if "TIT2" in audio.tags:
                    title = audio.tags["TIT2"].text[0]
        except Exception as e:
            print(f"Error updating song metadata for {song_path}: {e}")
        # Always update the label to ensure it reflects the current state
        self.now_playing_label.config(text=f"Now playing: {title} - {artist}")

    def update_play_count(self, song_path):
        self.play_counts[song_path] = self.play_counts.get(song_path, 0) + 1
        print(f"Play count for {os.path.basename(song_path)}: {self.play_counts[song_path]}")

    def toggle_shuffle(self):
	    self.shuffle = not self.shuffle  # Toggle the shuffle state

	    # Update button appearance based on the shuffle state
	    if self.shuffle:
		    self.shuffle_button.config(bg="#ffcc00")  # Highlighted color to indicate shuffle is on
	    else:
		    self.shuffle_button.config(bg="#00b386")  # Default color

    def toggle_shuffle(self):
        self.shuffle = not self.shuffle  # Toggle the shuffle state

        # Update button appearance based on the shuffle state
        if self.shuffle:
            self.shuffle_button.config(text="🔀 On", bg="#ffcc00")  # Highlighted color to indicate shuffle is on
        else:
            self.shuffle_button.config(text="Shuffle", bg="#00b386")  # Default color

    def toggle_repeat(self):
        repeat_modes = [0, 1, 3, 10]  # No repeat, once, thrice, ten times
        self.repeat_state = (self.repeat_state + 1) % len(repeat_modes)
        self.repeat_count = repeat_modes[self.repeat_state]

        # Update the button text based on the repeat state
        repeat_symbols = ["Repeat", "🔂 x1", "🔁 x3", "🔁 x10"]
        self.repeat_button.config(text=repeat_symbols[self.repeat_state])

        # todo might want to call a method to actually apply the repeat state to the player

    def check_events(self):
        for event in pygame.event.get():
            if event.type == self.SONG_END:
                self.play_next_song()
        self.root.after(100, self.check_events)  # Re-check every 100 milliseconds

    def toggle_pause(self):
        if self.paused:
            mixer.music.unpause()
            self.paused = False
        else:
            mixer.music.pause()
            self.paused = True

            if self.paused:
                self.stop_visualizer()
            else:
                self.start_visualizer()

    def stop_song(self):
        mixer.music.stop()
        self.current_song_index = None  # Reset the current song index or handle it accordingly
        self.stop_visualizer()

    def prev_song(self):
        if self.playlist:  # Check if the playlist is not empty
            if self.current_song_index is not None:
                self.current_song_index = (self.current_song_index - 1) % len(self.playlist)
            else:
                self.current_song_index = len(self.playlist) - 1
            self.play_song(self.playlist[self.current_song_index])
        else:
            print("The playlist is empty. Please load songs first.")

    def next_song(self):
        if not self.playlist:  # Check if the playlist is not empty
            print("The playlist is empty. Please load songs first.")
            return

        if self.shuffle:
            # Generate a random index that's different from the current song index
            next_song_index = random.choice([i for i in range(len(self.playlist)) if i != self.current_song_index])
        else:
            # Move to the next song in the list, or wrap around to the first song
            next_song_index = (self.current_song_index + 1) % len(
                self.playlist) if self.current_song_index is not None else 0

        self.current_song_index = next_song_index  # Update the current song index
        self.play_song(self.playlist[next_song_index])  # Play the selected song

    def set_volume(self, volume_level):
        mixer.music.set_volume(float(volume_level) / 100)


if __name__ == "__main__":
    root = tk.Tk()
    app = MusicPlayer(root)
    app.create_visualizer()
    root.mainloop()

# todo speed up mock visualizer
# todo make song titles show up correctly in the listbox
# todo theme switcher
# todo simple equalizer
# todo star rating system
# todo sleep timer
# todo audio effects
# todo clickable mascot
